<?
class Core{

    public $configFiles = [
        "dbconnect", 
        "define",
        "init",
        "includemodules",
    ];

    public $arModules = [
        "database",
        "user",
        "components",
        "security",
    ];

    public $tags = [
        "<html><head>",
        "</head>",
        "</html>"
    ];
    public function __construct()
    {   

    }

    public function includeCore(){
        $this->includeConfig();
        $this->includeModules();
    }

    public function includeHeader(){
        global $config;
        $this->includeCore();

        if ($config["SHOW_TEMPLATE"] ==  1){
            $this->showHeader(TEMPLATE_NAME);
        }
    }

    public function showHeader($templateName){

        echo $this->tags[0];
        require_once($_SERVER["DOCUMENT_ROOT"]."/application/templates/".$templateName."/head.php");
        echo $this->tags[1];
        require_once($_SERVER["DOCUMENT_ROOT"]."/application/templates/".$templateName."/header.php");
      
    }

    public function showCss(){
        if (file_exists($_SERVER["DOCUMENT_ROOT"]."/application/templates/".TEMPLATE_NAME."/style.css")){
            echo '<link href="/application/templates/'.TEMPLATE_NAME.'/style.css'.'" rel="stylesheet" type="text/css">';
        }

        if (file_exists($_SERVER["DOCUMENT_ROOT"]."/application/templates/".TEMPLATE_NAME."/script.js")){
            echo '<script src="/application/templates/'.TEMPLATE_NAME.'/script.js"></script>';
        }
    }
    public function showFooter($templateName){
        require_once($_SERVER["DOCUMENT_ROOT"]."/application/templates/".$templateName."/footer.php");
        echo $this->tags[2];
    }

    public function includeConfig(){
        foreach ($this->configFiles as $configFile){
            $filePath = $_SERVER["DOCUMENT_ROOT"]."/application/config/".$configFile.".php";
            
            if (file_exists($filePath)){
                require_once $filePath;
            } else {
                echo "Configuraction file not found <br>";
            }
        }
    }

    public function includeModules(){
        foreach ($this->arModules as $module){
            Core::includeModule($module);
        }
    }

    public static function includeModule($moduleName){
        $fullFileName = $_SERVER["DOCUMENT_ROOT"]."/application/modules/".$moduleName."/include.php";
        if (file_exists($fullFileName)){
            require_once $fullFileName;
        } else {
            echo "Module ".$moduleName. " isset error or is empty ";
        }
    }

    public function includeInstallFiles(){
        foreach ($this->arModules as $module){
            Core::includeInstallFile($module);
        }
    }

    public static function includeInstallFile($moduleName){
        $fullFileName = $_SERVER["DOCUMENT_ROOT"]."/application/modules/".$moduleName."/install.php";
        if (file_exists($fullFileName)){
            require_once $fullFileName;
        }
    }
}

?>
<?
class ConstructDB extends DataBase{

    public $dbconnect = [];
    public $tables = [];

    public $connectionsFromTables = [];

    public $defaultValues = [];


    public function __construct()
    {   
        global $dbconnect;
        $this->dbconnect = $dbconnect;

        global $installTables;
        $installTables = array_merge($installTables, $this->tables);

        global $connectionsFromTables;
        $connectionsFromTables = array_merge($connectionsFromTables, $this->connectionsFromTables);
        
        global $defaultValues;
        if (!empty($this->defaultValues)){
            foreach ($this->defaultValues as $key=> $values){
                foreach ($values as $value){
                    $defaultValues[$key][] = $value;
                }
            }
        }
    }
    public function createDataBase($dataBaseName){
        $query = "CREATE DATABASE " . $dataBaseName . " CHARACTER  SET utf8 COLLATE utf8_general_ci";
    
        $dbconnect = $this->dbconnect;

        unset($dbconnect["database"]);

        $result = $this->sendQuery($query, $dbconnect);
        return $result;
    }

    public function createTable($tableName, $arFields){
        
        $query = "CREATE TABLE `". $tableName . "` (";
        foreach ($arFields as $name => $type){
            $query .= @"$name $type, ";
        }

        $query = substr($query, 0, -2);

        $query .= ");";
        
        $result = $this->sendQuery($query, $this->dbconnect);
        return $result;
    }

    public function dropDataBase($dataBaseName){
        $query = "DROP DATABASE " . $dataBaseName;
        $result = $this->sendQuery($query, $this->dbconnect);
        return $result;
    }

    public function install($dataBaseName){
        $result = $this->dropDataBase($dataBaseName);
        $result = $this->createDataBase($dataBaseName);
        $result = $this->createTables();
        $result = $this->createConnectionFromTables();
        $this->setDefaultValues();
       
    }

    public function setDefaultValues(){


        foreach ($this->defaultValues as $table_name => $item){
            foreach ($item as $key => $default){
                $ORM = new ORM(); 
                $ORM->insertIntoTable($table_name, $default); 
            }
        }
    }

    public function createTables(){

        if (!empty($this->tables)){
            foreach ($this->tables as $tableName => $fields){
                $result = $this->createTable($tableName, $fields);
            }
        }
    }

    //ALTER TABLE `user` ADD FOREIGN KEY (`groupID`) REFERENCES `group`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

    public function createConnectionFromTables(){
        
        if (!empty($this->connectionsFromTables)){
            foreach ($this->connectionsFromTables as $connectionsFromTables){
                $query = @"ALTER TABLE `".$connectionsFromTables["INSIDE"]["TABLE"]."` ADD FOREIGN KEY (`".$connectionsFromTables["INSIDE"]["KEY"]."`) REFERENCES `".$connectionsFromTables["FORIGEN"]["TABLE"]."`(`".$connectionsFromTables["FORIGEN"]["KEY"]."`) ON DELETE CASCADE ON UPDATE CASCADE;";
                $result = $this->sendQuery($query, $this->dbconnect);
            }
        }
    }
    
    public function sendQuery($query, $params){
          
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        try{
            $mysqli = new mysqli($params["server"], $params["login"], $params["password"], $params["database"]);
            $obj = $mysqli->query($query, MYSQLI_USE_RESULT);
  
           //echo "<br>Success: ".$query;
            if (is_object($obj)){
                while($res = $obj->fetch_assoc()){
                    $result[] = $res;
                }
                $obj->close();

            } else {
                $result = $obj;
            }   
            $mysqli->close(); 
        }
        catch(Exception $e){
            //echo "<br>Error: ".$query;
        }
        return $result;
    }
    
    public function getListAllTables(){
        $query = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='".$this->dbconnect["database"]."'";
        $result = $this->sendQuery($query, $this->dbconnect);
        return $result;
    }
}
?>
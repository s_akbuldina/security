<?
    class InstallDB extends ConstructDB{

      
        public $connectionsFromTables = [];
        /*public $connectionsFromTables = [
            [
                "INSIDE" => [
                    "TABLE" => "lang_columns",
                    "KEY" => "lang_tables",
                ],
                "FORIGEN" => [
                    "TABLE" => "lang_tables",
                    "KEY" => "id",
                ]
            ],
        ];*/
        
        
        public function install($dataBaseName){
            $result = $this->dropDataBase($dataBaseName);
            $result = $this->createDataBase($dataBaseName);
            $result = $this->createTables();
            $result = $this->createConnectionFromTables();
        }

        public function createTables(){

            foreach ($this->tables as $tableName => $fields){
                
                $result = $this->createTable($tableName, $fields);
              
            }
        }

        //ALTER TABLE `user` ADD FOREIGN KEY (`groupID`) REFERENCES `group`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

        public function createConnectionFromTables(){
            
            foreach ($this->connectionsFromTables as $connectionsFromTables){
                $query = @"ALTER TABLE `".$connectionsFromTables["INSIDE"]["TABLE"]."` ADD FOREIGN KEY (`".$connectionsFromTables["INSIDE"]["KEY"]."`) REFERENCES `".$connectionsFromTables["FORIGEN"]["TABLE"]."`(`".$connectionsFromTables["FORIGEN"]["KEY"]."`) ON DELETE CASCADE ON UPDATE CASCADE;";
                $result = $this->sendQuery($query, $this->dbconnect);
            }
        }
    }
?>
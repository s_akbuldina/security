<?
    class InstallCore extends InstallDB{

        public $tables = [
            "lang_tables" => [
                "id" => "INT AUTO_INCREMENT",
                "name" => "VARCHAR(255)",
                "en" => "VARCHAR(255)",
                "ru" => "VARCHAR(255)",
                "PRIMARY KEY" => "(id)",
            ],
            "lang_columns" => [
                "id" => "INT AUTO_INCREMENT",
                "lang_tables" => "VARCHAR(255)",
                "name" => "VARCHAR(255)",
                "en" => "VARCHAR(255)",
                "ru" => "VARCHAR(255)",
                "PRIMARY KEY" => "(id)",
            ], 
            "file_fields" => [
                "id" => "INT AUTO_INCREMENT",
                "name" => "VARCHAR(255)",
                "table_name" => "VARCHAR(255)",
                "PRIMARY KEY" => "(id)",
            ],
            "date_fields" => [
                "id" => "INT AUTO_INCREMENT",
                "name" => "VARCHAR(255)",
                "table_name" => "VARCHAR(255)",
                "PRIMARY KEY" => "(id)",
            ]
        ];

       
        public $defaultValues = [
            "lang_tables" => [
                [
                    "name" => "lang_columns",
                    "en" => "lang columns",
                    "ru" => "Языковые настройки полей",
                ], 
                [
                    "name" => "lang_tables",
                    "en" => "Lang tables",
                    "ru" => "Языковые настройки таблиц",
                ], 
                [
                    "name" => "file_fields",
                    "en" => "File fields",
                    "ru" => "Файловые поля",
                ], 
            ],   
            "lang_columns" => [
                [
                    "lang_tables" => "lang_tables",
                    "name" => "id",
                    "en" => "ID",
                    "ru" => "Код",
                ],
                [
                    "lang_tables" => "lang_tables",
                    "name" => "name",
                    "en" => "Name in DB",
                    "ru" => "Название в БД",
                ],
                [
                    "lang_tables" => "lang_tables",
                    "name" => "en",
                    "en" => "En Name",
                    "ru" => "Название на англ",
                ],
                [
                    "lang_tables" => "lang_tables",
                    "name" => "ru",
                    "en" => "Ru Name",
                    "ru" => "Название таблиц",
                ],
                [
                    "lang_tables" => "lang_columns",
                    "name" => "id",
                    "en" => "ID",
                    "ru" => "Код",
                ],
                [
                    "lang_tables" => "lang_columns",
                    "name" => "lang_tables",
                    "en" => "Name in DB",
                    "ru" => "Таблица в БД",
                ],
                [
                    "lang_tables" => "lang_columns",
                    "name" => "name",
                    "en" => "Field name",
                    "ru" => "Название поля",
                ],
                [
                    "lang_tables" => "lang_columns",
                    "name" => "en",
                    "en" => "En Field name",
                    "ru" => "Название поля на англ",
                ],
                [
                    "lang_tables" => "lang_columns",
                    "name" => "ru",
                    "en" => "Ru Field name",
                    "ru" => "Название поля на русском",
                ],
                [
                    "lang_tables" => "file_fields",
                    "name" => "id",
                    "en" => "ID",
                    "ru" => "ID",
                ],
                [
                    "lang_tables" => "file_fields",
                    "name" => "name",
                    "en" => "Field",
                    "ru" => "Названия поля",
                ],
                [
                    "lang_tables" => "file_fields",
                    "name" => "table_name",
                    "en" => "Table",
                    "ru" => "Название таблицы",
                ], 
            ]
        ];

        public function __construct()
        {   
            global $dbconnect;
            $this->dbconnect = $dbconnect;
    
            global $installTables;
            $installTables = array_merge($installTables, $this->tables);
    
            global $connectionsFromTables;
            $connectionsFromTables = array_merge($connectionsFromTables, $this->connectionsFromTables);
            
            global $defaultValues;
         
            $defaultValues = array_merge($defaultValues, $this->defaultValues);
            
        }
    }
?>
<?
    $arFields = [
        "lib" => [
            "DataBase", 
            "orm",
        ],
        "install" => [
            "constructdb",
            "installdb",
            "installcore",
        ]
    ];
    $Load = new AutoLoader("database");
    $Load->includeClass($arFields);
?>
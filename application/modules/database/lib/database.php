<?

    class DataBase{
        
        public $server;
        public $dataBase;
        public $login;
        public $password; 
        public $status = ["status" => true];

        public $dbconnect = [];

        public function __construct()
        {   
            global $dbconnect;
            $this->dbconnect = $dbconnect;
        }


        public function connect(){
            $this->checkConnect();
            $this->checkConnectDataBase();
            return $this->status;
        }

        public function checkConnect(){
            global $dbconnect;
            $link = mysqli_connect($dbconnect["server"], $dbconnect["login"], $dbconnect["password"]);

            if ($link == false){
                $this->checkStatus("Ошибка: Невозможно подключиться к MySQL " . mysqli_connect_error());
            }
        }

        public function checkConnectDataBase(){
            global $dbconnect;
            $link = mysqli_connect($dbconnect["server"], $dbconnect["login"], $dbconnect["password"], $dbconnect["database"]);
            if ($link == false){
                $this->checkStatus("Ошибка: Невозможно подключиться к БД " . mysqli_connect_error());
            }
        }

        public function checkStatus($message = false){
            if (empty($this->status["message"])){
                if ($message){
                    $this->status["status"] =  false;
                    $this->status["message"] =  $message;
                }
            }
        }

        
        public function sendQuery($query, $params){
          
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
            try{
                $mysqli = new mysqli($params["server"], $params["login"], $params["password"], $params["database"]);
                $obj = $mysqli->query($query, MYSQLI_USE_RESULT);
      
              // echo "<br>Success: ".$query;
                if (is_object($obj)){
                    while($res = $obj->fetch_assoc()){
                        $result[] = $res;
                    }
                    $obj->close();
    
                } else {
                    $result = $obj;
                }   
                $mysqli->close();
                
            }
            catch(Exception $e){
                //echo "<br>Error: ".$query;
            }
            return $result;
        }
    
    }
?>
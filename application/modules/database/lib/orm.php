<?
    class ORM extends DataBase{

        public $dbconnect = [];
        public $tablesName = [];
        public $foregin = [];
        public $informationSchema = [];
        public $keyColumnUsage = [];

        public function __construct()
        {   
            global $dbconnect;
            $this->dbconnect = $dbconnect;
        }

        public function getKeyColumnUsage($table){
            $query = @"SELECT * FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE TABLE_NAME = '$table' and TABLE_SCHEMA = '".$this->dbconnect["database"]."'";
            $result = $this->sendQuery($query, $this->dbconnect);
            $this->keyColumnUsage = $result;
            return $result;
        }
        public function getForegin(){
            foreach ($this->keyColumnUsage as $column){
                if (isset($this->informationSchema[$column["COLUMN_NAME"]])){
                    if (!empty($column["REFERENCED_TABLE_NAME"])){
                        $this->foregin[$column["REFERENCED_TABLE_NAME"]]["column"] = $column["REFERENCED_COLUMN_NAME"];
                    }
                }
            }
            return $this->foregin;
        }

        public function getInformationSchema($table){
            $query = @"SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$table' and TABLE_SCHEMA = '".$this->dbconnect["database"]."' ORDER BY `COLUMNS`.`ORDINAL_POSITION` ASC";
            $property = $this->sendQuery($query, $this->dbconnect);
 
            $columns = [];
            foreach ($property as $column){
                $columns[$column["COLUMN_NAME"]] = $column["DATA_TYPE"];
            }
            $this->informationSchema = $columns;

            return $columns;
        }
        public function insertIntoTable($table, $arFields){

            $query = @"SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$table'";
            $property = $this->sendQuery($query, $this->dbconnect);
          
            $columns = [];
            foreach ($property as $column){
                $columns[$column["COLUMN_NAME"]] = $column["DATA_TYPE"];
            }

            $query = @"INSERT INTO `$table` ";

            $keys = "(";
            $values = "(";
            foreach ($arFields as $key => $value){
                if (!empty($value)){
                    if (!empty($columns[$key])){
                        $keys .= @"$key, ";
                        if ($columns[$key] == "int"){
                            $values .= @"$value, ";
                        } else {
                            $values .= @"'$value', ";
                        }
                    }
                }
                
            }
            $keys = substr($keys, 0, -2);
            $values = substr($values, 0, -2);
            $keys .= ")";
            $values .= ")";
            $query .= @"$keys VALUES $values";
            $result = $this->sendQuery($query, $this->dbconnect);
            return $result;
        }
        
        public function updateTableByID($table, $values, $where){
            
            $result = false;

            $query = @"SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$table'";
            $property = $this->sendQuery($query, $this->dbconnect);
          
            $columns = [];
            foreach ($property as $column){
                $columns[$column["COLUMN_NAME"]] = $column["DATA_TYPE"];
            }

            $query = @"UPDATE $table SET ";
            $set = "";
            foreach ($values as $key => $value){
                if (!empty($columns[$key])){
                    if ($columns[$key] == "int"){
                        $set .= @" $key=$value,";
                    } else {
                        $set .= @" $key='$value',";
                    }
                }
            }
            $set = substr($set, 0, -1);

            $query .= $set;

            $query .= @" WHERE id=$where";
            $result = $this->sendQuery($query, $this->dbconnect);
            
            if (strlen($result) > 0){
                $result = true;
            } else {
                $result = false;
            }
            return $result;
        }

        public function selectFromTable($table, $selectFields, $arFilter){
            $query = "SELECT ";
            if (!empty($selectFields)){

            } else {
                $query .= "* ";
            }

            $query .= @"FROM $table ";
            
            $result = $this->sendQuery($query, $this->dbconnect);

            return $result;
        }

        public function deleteFromTable($table, $where){
            $query = @"DELETE FROM $table WHERE id=$where";
            $result = $this->sendQuery($query, $this->dbconnect);
        }

        public function dropTable($table){

        }

        public function checkStructure(){
            $this->checkTablesName(); 

        }

        public function checkTablesName(){
           
            $query = @" SHOW TABLES";
            $result = $this->sendQuery($query, $this->dbconnect);

            foreach ($result as $key => $column){
                foreach ($column as $tableName){
                    $this->tablesName[$tableName] = $tableName;  
                }
            }
        }
        
        public function getList($table, $select, $filter){

            if (!empty($table)){
                $query = @"SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$table'";
                $property = $this->sendQuery($query, $this->dbconnect);
                
                $columns = [];
                foreach ($property as $column){
                    $columns[$column["COLUMN_NAME"]] = $column["DATA_TYPE"];
                }
    
                $strSelect = "";
                if (is_array($select)){
                    if (count($select) > 0){
                        foreach ($select as $item){
                            if (!empty($columns[$item])){
                                $strSelect .= $item.", ";
                            }
                        }
                        $strSelect = substr($strSelect, 0, -2);
                    } else {
                        $strSelect = "*";
                    }
                } else {
                    $strSelect = "*"; 
                }
       
                $strWhere = "";
                if (is_array($filter)){
                    if (count($filter) > 0){
                        $strWhere = " WHERE ";
                        foreach ($filter as $key => $item){
                            if (!empty($columns[$key])){
                                $keys .= @"$key, ";
                                if ($columns[$key] == "int"){
                                    if ($key == array_key_first($filter)){
                                        $strWhere .= @"$key=$item";
                                    } else {
                                        $strWhere .= @" AND $key=$item";
                                    }
                                } else {
                                    if ($key == array_key_first($filter)){
                                        $strWhere .= @"$key='$item'";
                                    } else {
                                        $strWhere .= @" AND $key='$item'";
                                    }
                                }
                            }
                        }
                    }
                }
                $query = @"SELECT $strSelect FROM $table $strWhere";
                $result = $this->sendQuery($query, $this->dbconnect);
                return $result;
            } 
        }
    }
?>
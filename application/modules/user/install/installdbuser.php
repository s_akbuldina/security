<?
    class InstallDBUser extends InstallDB{

        public $tables = [
            "user_group" => [
                "id" => "INT AUTO_INCREMENT",
                "group_code" => "VARCHAR(255)",
                "name" => "VARCHAR(255)",
                "PRIMARY KEY" => "(id)",
            ],
            "user" => [
                "id" => "INT AUTO_INCREMENT",
                "name" => "VARCHAR(255)",
                "last_name" => "VARCHAR(255)",
                "user_group" => "INT",
                "login" => "VARCHAR(255)",
                "password" => "VARCHAR(255)",
                "phone" => "VARCHAR(255)",
                "email" => "VARCHAR(255)",
                "PRIMARY KEY" => "(id)",
            ],
            "access" => [
                "id" => "INT AUTO_INCREMENT",
                "name" => "VARCHAR(255)",
                "attribute" => "VARCHAR(255)",
                "desctiption" => "VARCHAR(255)",
                "PRIMARY KEY" => "(id)",
            ],
            "access_table" => [
                "id" => "INT AUTO_INCREMENT",
                "name" => "VARCHAR(255)", //Название таблиц
                "access" => "INT",
                "user_group" => "INT",
                "PRIMARY KEY" => "(id)",
            ],
            
        ];

        public $connectionsFromTables = [
            [
                "INSIDE" => [
                    "TABLE" => "user",
                    "KEY" => "user_group",
                ],
                "FORIGEN" => [
                    "TABLE" => "user_group",
                    "KEY" => "id",
                ]
            ],
            [
                "INSIDE" => [
                    "TABLE" => "access_table",
                    "KEY" => "user_group",
                ],
                "FORIGEN" => [
                    "TABLE" => "user_group",
                    "KEY" => "id",
                ]
            ],
            [
                "INSIDE" => [
                    "TABLE" => "access_table",
                    "KEY" => "access",
                ],
                "FORIGEN" => [
                    "TABLE" => "access",
                    "KEY" => "id",
                ]
            ],
        ];

        public $defaultValues = [
            "user_group" => [
                [
                    "group_code" => "admin",
                    "name" => "Администраторы",
                ], 
                [
                    "group_code" => "employees",
                    "name" => "Сотрудники",
                ],
                [
                    "group_code" => "clients",
                    "name" => "Клиенты",
                ], 
            ],
            "user" => [
                [
                    "name" => "admin",
                    "last_name" => "admin", 
                    "user_group" => "1",
                    "login" => "admin", 
                    "password" => "123456"
                ],
                [
                    "name" => "employees",
                    "last_name" => "employees", 
                    "user_group" => "2",
                    "login" => "employees", 
                    "password" => "123456"
                ],
                [
                    "name" => "client",
                    "last_name" => "client", 
                    "user_group" => "3",
                    "login" => "client", 
                    "password" => "123456"
                ]
            ],
            "lang_tables" => [
                [
                    "name" => "user_group",
                    "en" => "user_group",
                    "ru" => "Группы",
                ],
                [
                    "name" => "user",
                    "en" => "user",
                    "ru" => "Пользователи",
                ]
            ],
          
            "access" => [
                [
                    "name" => "Чтение",
                    "attribute" => "R",
                    "desctiption" => "Чтение",
                ],
                [
                    "name" => "Выполнение",
                    "attribute" => "X",
                    "desctiption" => "Выполнение",
                ],
                [
                    "name" => "Запрет",
                    "attribute" => "D",
                    "desctiption" => "Запрет",
                ],
                [
                    "name" => "Запись",
                    "attribute" => "W",
                    "desctiption" => "Запрет",
                ],
            ],
            "lang_columns" => [
                [
                    "lang_tables" => "user_group",
                    "name" => "id",
                    "en" => "ID",
                    "ru" => "Код",
                ],
                [
                    "lang_tables" => "user_group",
                    "name" => "group_code",
                    "en" => "CODE",
                    "ru" => "Символьный код",
                ],
                [
                    "lang_tables" => "user_group",
                    "name" => "name",
                    "en" => "Group name",
                    "ru" => "Название группы",
                ],
                [
                    "lang_tables" => "user",
                    "name" => "id",
                    "en" => "ID",
                    "ru" => "Код",
                ],
                [
                    "lang_tables" => "user",
                    "name" => "name",
                    "en" => "Name",
                    "ru" => "Имя",
                ],
                [
                    "lang_tables" => "user",
                    "name" => "last_name",
                    "en" => "Last name",
                    "ru" => "Фамилия",
                ],
                [
                    "lang_tables" => "user",
                    "name" => "user_group",
                    "en" => "Group",
                    "ru" => "Группа",
                ],
                [
                    "lang_tables" => "user",
                    "name" => "login",
                    "en" => "Login",
                    "ru" => "Логин",
                ],
                [
                    "lang_tables" => "user",
                    "name" => "password",
                    "en" => "Password",
                    "ru" => "Пароль",
                ],
                [
                    "lang_tables" => "user",
                    "name" => "phone",
                    "en" => "Phone",
                    "ru" => "Номер",
                ],
                [
                    "lang_tables" => "user",
                    "name" => "email",
                    "en" => "Email",
                    "ru" => "Email",
                ],
                [
                    "lang_tables" => "access",
                    "name" => "name",
                    "en" => "Name",
                    "ru" => "Доступ",
                ],
                [
                    "lang_tables" => "access",
                    "name" => "desctiption",
                    "en" => "Desctiption",
                    "ru" => "Описание",
                ],
            ], 
            "access_table" => [
                [
                    "name" => "lead", 
                    "access" => 4, 
                    "user_group" => 3,  
                ]
             
            ]

        ];

        public function __construct()
        {   
            global $dbconnect;
            $this->dbconnect = $dbconnect;
    
            global $installTables;
            $installTables = array_merge($installTables, $this->tables);
    
            global $connectionsFromTables;
            $connectionsFromTables = array_merge($connectionsFromTables, $this->connectionsFromTables);
            
            global $defaultValues;
            
            $tables = $this->getListAllTables();
            
            if (!empty($this->defaultValues)){
               
                foreach ($this->defaultValues as $key=> $values){
                    foreach ($values as $value){
                        $defaultValues[$key][] = $value;
                    }
                }
                foreach ($tables as $table){
                    $defaultValues["access_table"][] = [
                        "name" => $table["TABLE_NAME"], 
                        "access" => 2, 
                        "user_group" => 1, 
                    ];
                    $defaultValues["access_table"][] = [
                        "name" => $table["TABLE_NAME"], 
                        "access" => 2, 
                        "user_group" => 2, 
                    ];
                }
            }
            
        }
    }
?>
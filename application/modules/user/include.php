<?
    $arFields = [
        "install" => [
            "installdbuser",
        ],
        "lib" => [
            "ormuser",
            "user",
        ]
    ];
    $Load = new AutoLoader("user");
    $Load->includeClass($arFields);
    global $User;

    $User = new User();
    if (isset($_REQUEST["logout"]) && $_REQUEST["logout"] == "yes"){
        unset($_SESSION["user_status"]);
    }
    if (empty($_SESSION["user_status"]) && !isset($_REQUEST["login"])){
        if ($_SERVER["REQUEST_URI"] != "/auth/" && $_SERVER["REQUEST_URI"] != "/reg/"){
            header('Location: /auth/');
        }
    } 
?>
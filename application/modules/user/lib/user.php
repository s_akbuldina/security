<?

    class User extends ORMUser{
        
       
        private $authorize = false;

        public function authorize($login, $password){
            $result = $this->checkUser($login, $password);
            if ($result["id"]){
                $result["status"] = "success";
                $_SESSION["user_id"] = $result["id"];
                $_SESSION["user_group"] = $result["user_group"];
                $_SESSION["user_status"] = true;
            }
            return $result;
        }
        public function isAuthorize(){
            if ($_SESSION["user_status"]){
                $this->authorize = true;
                $this->authorize = false;
            } else {
                $this->authorize = false;
            }
            return $this->authorize;
        }

        public function authorizeUser($id){
            
            $_SESSION["user_status"] = true;
        }

        public function isAdmin(){
            
            if ($_SESSION["user_status"]){
                if ($this->getUserGroup() == "admin"){
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
?>
<?
    class Components {

        public $componentName;
        public $componentTemplate;
        public $arComponent;
        public $params;
        public $result;
        public $parentComponentName = [];
        public $parentcomponentTemplate = [];
        public $parentarResult = [];
        public $parentParams = [];

        public function includeComponents($componentName, $componentTemplate = 0, $params = 0){
            if (!empty($this->componentName)){
                $this->parentComponentName[] = $this->componentName;
                $this->parentcomponentTemplate[] = $this->componentTemplate;
                $this->parentarResult[] = $this->arResult;
                $this->parentParams[] = $this->params;
                unset($this->componentName);
                unset($this->componentTemplate);
                unset($this->arResult);
                unset($this->params);
            }
            $this->componentName = $componentName;
            $this->componentTemplate = $componentTemplate;
            $this->params = $params;
            if (file_exists($_SERVER["DOCUMENT_ROOT"]."/application/components/".$this->componentName.'/component.php')){
                require($_SERVER["DOCUMENT_ROOT"]."/application/components/".$componentName.'/component.php');
            } else {
                echo "<br>Компонент не обнаружен";
            }
            $lastKey = array_key_last($this->parentComponentName);
            $this->componentName = $this->parentComponentName[$lastKey];
            $this->componentTemplate = $this->parentcomponentTemplate[$lastKey];
            $this->arResult = $this->parentarResult[$lastKey];
            $this->params = $this->parentParams[$lastKey];

            unset($this->parentComponentName[$lastKey]);
            unset($this->parentcomponentTemplate[$lastKey]);
            unset($this->parentarResult[$lastKey]);
            unset($this->parentParams[$lastKey]);
        }
        
        private function includeTemplateComponents($template = "template"){
            global $config;
            if ($config["SHOW_TEMPLATE"] == 1){
                $arResult = $this->arResult;
                $params = $this->params;
                if (file_exists($_SERVER["DOCUMENT_ROOT"]."/application/components/".$this->componentName.'/templates/'.$this->componentTemplate.'/'.$template.'.php')){
                    require($_SERVER["DOCUMENT_ROOT"]."/application/components/".$this->componentName.'/templates/'.$this->componentTemplate.'/'.$template.'.php');
                } else {
                    echo "<br>Шаблон компонента не обнаружен";
                }
            
                $this->showCss();
                $this->showJs();
            }
        }

        private function showCss(){
            if (file_exists($_SERVER["DOCUMENT_ROOT"]."/application/components/".$this->componentName.'/templates/'.$this->componentTemplate.'/style.css')){
                echo '<link href="/application/components/'.$this->componentName.'/templates/'.$this->componentTemplate.'/style.css"  rel="stylesheet" type="text/css">';
            }
        }

        private function showJs(){
            if (file_exists($_SERVER["DOCUMENT_ROOT"]."/application/components/".$this->componentName.'/templates/'.$this->componentTemplate.'/script.js')){
                echo '<script src="/application/components/'.$this->componentName.'/templates/'.$this->componentTemplate.'/script.js"></script>';
            }
        }
    }
?>
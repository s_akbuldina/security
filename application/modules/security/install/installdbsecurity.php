<?
    class InstallDBSecurity extends InstallDB{

        public $tables = [
            "lead" => [
                "id" => "INT AUTO_INCREMENT",
                "name" => "VARCHAR(255)",
                "address" => "VARCHAR(255)",
                "working_mode" => "VARCHAR(255)",
                "type_protection" => "INT",
                "message" => "VARCHAR(255)",
                "user" => "INT",
                "file" => "VARCHAR(255)",
                "PRIMARY KEY" => "(id)",
            ], 
            "type_protection" => [
                "id" => "INT AUTO_INCREMENT",
                "name" => "VARCHAR(255)",
                "PRIMARY KEY" => "(id)",
            ],
            
            
        ];
        public $defaultValues = [
          
            "file_fields" => [
                [
                    "name" => "file",
                    "table_name" => "lead",
                ],
               
            ],
            "lang_tables" => [
                [
                    "name" => "lead",
                    "en" => "lead",
                    "ru" => "Заявки",
                ],
                [
                    "name" => "type_protection",
                    "en" => "Type protection",
                    "ru" => "Вид охраны",
                ],
            ],
            "lang_columns" => [
                [
                    "lang_tables" => "lead",
                    "name" => "id",
                    "en" => "ID",
                    "ru" => "Код",
                ],
                [
                    "lang_tables" => "lead",
                    "name" => "name",
                    "en" => "Name",
                    "ru" => "Название организации",
                ],
                [
                    "lang_tables" => "lead",
                    "name" => "address",
                    "en" => "Address",
                    "ru" => "Адрес",
                ],
                [
                    "lang_tables" => "lead",
                    "name" => "working_mode",
                    "en" => "Working Mode",
                    "ru" => "Режим работы",
                ],
                [
                    "lang_tables" => "lead",
                    "name" => "type_protection",
                    "en" => "Type Protection",
                    "ru" => "Вид охраны",
                ],
                [
                    "lang_tables" => "lead",
                    "name" => "message",
                    "en" => "Message",
                    "ru" => "Сообщение",
                ],
                [
                    "lang_tables" => "lead",
                    "name" => "user",
                    "en" => "Client",
                    "ru" => "Клиент",
                ],
                [
                    "lang_tables" => "lead",
                    "name" => "file",
                    "en" => "File",
                    "ru" => "Файл",
                ],
                [
                    "lang_tables" => "type_protection",
                    "name" => "id",
                    "en" => "ID",
                    "ru" => "Код охраны",
                ],
                [
                    "lang_tables" => "type_protection",
                    "name" => "name",
                    "en" => "Name",
                    "ru" => "Вид охраны",
                ],
            ],
            "type_protection" => [
                [
                    "name" => "ОС - Охранная сигнализация",
                ],
                [
                    "name" => "КТС - Кнопка тревожной сигнализации",
                ],
                [
                    "name" => "OC + КТС",
                ],
            ],
        ];
        public $connectionsFromTables = [
            [
                "INSIDE" => [
                    "TABLE" => "lead",
                    "KEY" => "type_protection",
                ],
                "FORIGEN" => [
                    "TABLE" => "type_protection",
                    "KEY" => "id",
                ]
            ],
            [
                "INSIDE" => [
                    "TABLE" => "lead",
                    "KEY" => "user",
                ],
                "FORIGEN" => [
                    "TABLE" => "user",
                    "KEY" => "id",
                ]
            ],
        ];
        
        

        public function __construct()
        {   
            global $dbconnect;
            $this->dbconnect = $dbconnect;
    
            global $installTables;
            $installTables = array_merge($installTables, $this->tables);
    
            global $connectionsFromTables;
            $connectionsFromTables = array_merge($connectionsFromTables, $this->connectionsFromTables);
            
            global $defaultValues;
            
            $tables = $this->getListAllTables();
            
            if (!empty($this->defaultValues)){
               
                global $defaultValues;
                if (!empty($this->defaultValues)){
                    //$defaultValues = array_merge($defaultValues, $this->defaultValues);
                    foreach ($this->defaultValues as $key=> $values){
                        foreach ($values as $value){
                            $defaultValues[$key][] = $value;
                        }
                    }
                }
            }
            
        }
    }
?>
<?
    global $User; 
if (!$User->isAuthorize()):?>
    <div class="form">
        <form class="auth-form">
            <div class="form-header">
                <h1>Вход в профиль</h1>
            </div>
            <input class="submit-item" type="hidden" name="SHOW_TEMPLATE" value="0">

            <div class="form-item">
                <label>Логин</label>
                <input class="input-item" type="text" name="login">
            </div>
            <div class="form-item">
                <label>Пароль</label>
                <input class="input-item" type="password" name="password">
            </div>
            <div class="form-item">
                <input class="submit-item" type="submit" value="Авторизоваться">
            </div>
            <div class="status">
            </div>
           
        </form>
        <a href="/reg">
            <div class="reg">
                Регистрация
            </div>
        </a>
    </div>
<?endif?>
<?  $orm = new ORM();
    $access_table = $orm->getList("access_table", [], ["user_group" => $_SESSION["user_group"], "name" => $this->params["table"]])[0];
    $access = $orm->getList("access", [], ["id" => $access_table["access"]])[0];
?>
    <?if ($access["attribute"] == "X" || $access["attribute"] == "W"):?>
        <?if (!empty($arResult)):?>
            <?global $config?>
            <div class=" d-flex w-100 justify-content-center">
                <div class="table-form col-10 row">
                    <div class="header">
                        <div class="title">
                            <h2>Создание заявки</h2>
                        </div>
                    </div>
                    <form class="w-100" method="post" name="table">
                        <?foreach ($arResult["lang_columns"] as $column):?>
                            <?if ($arResult["COLUMN"][$column["name"]] == "TEXT"):?>
                                <div class="w-100">
                                    <div class="w-100">
                                        <span>
                                            <?=$column[$config["LANG"]]?>
                                        </span>
                                    </div>
                                    <div class="w-100">
                                        <input class="w-100" type="text" name="<?=$column["name"]?>" value="">
                                    </div>
                                </div>
                            <?endif?>
                            <?if ($arResult["COLUMN"][$column["name"]] == "LIST"):?>
                                <div class="w-100">
                                    <div class="w-100">
                                        <span>
                                            <?=$column[$config["LANG"]]?>
                                        </span>
                                    </div>
                                    <div class="w-100">
                                        <select class="w-100" name="<?=$column["name"]?>">
                                            <?foreach ($arResult["foreginData"][$column["name"]] as $foreginData):?>
                                                <option value="<?=$foreginData[$arResult["foregin"][$column["name"]]["column"]]?>" <?if ($foreginData[$arResult["foregin"][$column["name"]]["column"]] == $arResult["default_values"][$column["name"]]):?>selected<?endif?>><?=$foreginData["name"]?></option>
                                            <?endforeach?>
                                        </select>
                                    </div>
                                </div>
                            <?endif?>
                            <?if ($arResult["COLUMN"][$column["name"]] == "FILE"):?>
                                <div class="w-100">
                                    <div class="w-100">
                                        <span>
                                            <?=$column[$config["LANG"]]?>
                                        </span>
                                    </div>
                                    <div id="uploads">
                                        <ul>

                                        </ul>
                                    </div>
                                    <div class="w-100 d-flex justify-content-center">
                                        <div class="dropzone" name="<?=$column["name"]?>" id="dropzone">
                                            Документ
                                        </div>
                                    </div>
                                </div>
                            <?endif?>
                            <?if ($arResult["COLUMN"][$column["name"]] == "DATE"):?>
                                <div class="w-100">
                                    <div class="w-100">
                                        <span>
                                            <?=$column[$config["LANG"]]?>
                                        </span>
                                    </div>
                                    <div class="w-100">
                                        <input class="w-100 calendar" type="text" name="<?=$column["name"]?>" value="">
                                    </div>
                                </div>
                            <?endif?>
                            <?if ($arResult["COLUMN"][$column["name"]] == "HIDDEN"):?>
                                <input class="w-100" type="hidden" name="<?=$column["name"]?>" value="<?=$arResult["default_values"][$column["name"]]?>">       
                            <?endif?>
                        <?endforeach?>
        
                        <input  type="hidden" name="handler" value="add">
                        <div class="w-100 d-flex justify-content-center m-2">
                            <button class="col-3 add-table">Добавить</button>
                        </div>
                    </form>
                </div>
            </div>
            <script>
                var tabledata;
                $(document).ready(function(){
                    tabledata = new tableData(<?=json_encode($arResult)?>);
                });
                
                $('.calendar').daterangepicker({
                    inline: true,
                    singleDatePicker: true,
                    startDate: moment().subtract(2, 'days'),
                    endDate: moment(),
                    autoApply: true,

                    minDate: moment().subtract(0, 'days'),
                    "locale": {
                        "format": "DD.MM.YYYY",
                        "separator": " - ",
                        "fromLabel": "From",
                        "toLabel": "To",
                        "customRangeLabel": "Custom",
                        "weekLabel": "W",
                        "daysOfWeek": ["ВС", "ПН", "ВТ",  "СР","ЧТ", "ПТ", "СБ" ],
                        "monthNames": ["Январь","Февраль","Март","Апрель", "Май", "Июнь","Июль","Август","Сентябрь","Октябрь", "Ноябрь","Декабрь"],
                        "firstDay": 1
                    },
                });
                $(document).ready(function () {
                    //$('.daterangepicker.dropdown-menu.ltr.show-calendar.opensright').show();
                });
            </script>
        <?endif?>
    <?else:?>
        Добавление запрещено
    <?endif?>

 class tableData{
    constructor(arResult) { 
        this.arResult = arResult;
        this.files = [];
    } 
    
    constructSelectTemplateHouse(ID, selector){
      
    }
    getUpdateUrl(){
        return this.arResult.update_url;
    }
    getDeleteUrl(){
        return this.arResult.delete_url;
    }
    setFile(file, fieldName){
        this.files[this.files.length] = [fieldName, file];
    }
    getFiles(){
        return this.files;
    }
    getTableName(){
        return this.arResult.table;
    }
    getCurPage(){
        return this.arResult.CurPage;
    }
    getAccess(){
        return this.arResult.access;
    }
}

 var formData = new FormData();
 document.addEventListener("DOMContentLoaded", function(event) {
    try{
        var data;
        (function() {
            var dropzone = document.querySelectorAll(".dropzone");
   
            Object.keys(dropzone).forEach(function(itemID){
                dropzone[itemID].ondrop = function(e) {
                    this.className = 'dropzone';
                    this.innerHTML = 'Документ';
                    e.preventDefault();
                    upload(e.dataTransfer.files, this.getAttribute("name"));
                };

                var displayUploads = function(data) {
                    var uploads = document.getElementById("uploads"),
                        anchor,
                        x;

                    for(x = 0; x < data.length; x++) {
                        anchor = document.createElement('li');
                        anchor.innerHTML = data[x].name;
                        uploads.appendChild(anchor);
                    }
                };

                var upload = function(files, fieldName) {
                    var xhr = new XMLHttpRequest(),
                    x;

                    tabledata.setFile(files, fieldName);

                    for(x = 0; x < files.length; x++) {
                       // formData.append('file[]', files[x]);
                        formData.append(fieldName, files[x]);
                    }

                    //xhr.onload = function() {
                      //  data = JSON.parse(this.responseText);
                    // displayUploads(data);
                    
                   // };
                
                    //xhr.open('post', '/upload.php', true);
                   // xhr.send(formData);
                };

                dropzone[itemID].ondragover = function() {
                    this.className = 'dropzone dragover';
                    this.innerHTML = 'Отпустите мышку';
                    
                    return false;
                };

                dropzone[itemID].ondragleave = function() {
                    this.className = 'dropzone';
                    this.innerHTML = 'Документ';
                    return false;
                };
            });
        }());
    } catch{
        
    }
 });


$(document).ready(function(){
    console.log(tabledata.getAccess());
    if (tabledata.getAccess() == "X"){
        $(".table-data .list").on("click", function(){
            var formID = $(this).attr("list-id");
            var form = $("[form-id=" + formID + "]");
            $(this).css({"display" : "none"});
            $(form).css({"display" : "flex"});
        });
        $(".table-data .cancel").on("click", function(e){
            e.preventDefault();
            var formID = $(this).attr("form-id");
            var form = $("[list-id=" + formID + "]");
            $("[form-id=" + formID + "]").css({"display" : "none"});
            $(form).css({"display" : "flex"});
        });
    }
  

    $(".save").on("click", function(e){
        e.preventDefault();
        var formID = $(this).attr("form-id");

        var location_href = tabledata.getCurPage();

        formData = new FormData(document.querySelectorAll("[data-id]")[formID]);
 
        formData.append('table', tabledata.getTableName());
        formData.append('handler', "update");
    
       
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            if (this.responseText){
                location.href = location_href;
            }
        };
        xhr.open('post', '/application/components/table/ajax.php', true);
        xhr.send(formData); 
    });

    $(".delete").on("click", function(e){
        e.preventDefault();
        var formID = $(this).attr("form-id");

        var location_href = tabledata.getCurPage();

        formData = new FormData(document.querySelectorAll("[data-id]")[formID]);

        formData.append('table', tabledata.getTableName());
        formData.append('handler', "delete");
    
       
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            location.href = location_href;
        };
        xhr.open('post', '/application/components/table/ajax.php', true);
        xhr.send(formData); 
       
    });
    
    $(".add-table").on("click", function(e){
        e.preventDefault();
        var table = tabledata.getTableName();
        var location_href = tabledata.getCurPage();

        formData.append('handler', "update");
        formData.append('table', table);
        formData.append('SHOW_TEMPLATE', "0");

        var input = document.querySelectorAll("input");
        Object.keys(input).forEach(function(itemID){
            formData.append(input[itemID].getAttribute("name"), input[itemID].value);
        });

        var select = document.querySelectorAll("select");
        Object.keys(select).forEach(function(itemID){
            formData.append(select[itemID].getAttribute("name"), select[itemID].value);
        });

        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            formData = new FormData();
            var data;
            data = JSON.parse(this.responseText);
            console.log(data);
            location.href = location_href;
        };
        xhr.open('post', '/application/components/table/ajax.php', true);
        xhr.send(formData); 
    });
});
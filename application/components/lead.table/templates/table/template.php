<?global $config;
    global $User;

?>
<?if (!empty($arResult["COLUMN"])):?>

    <div class="table">
        <div class="header">
            <div class="title">
                <h2><?=$arResult["LANG"]?></h2>
            </div>
           
        </div>
        <div class="table-work-space">
            <div class="table-data">
                <div class="columns-name">
                    <?foreach ($arResult["COLUMN"] as $column => $type):?>
                        <div class="table-item">
                            <span>
                                <?=$arResult["lang_columns"][$column][$config["LANG"]]?>
                            </span>
                        </div>
                    <?endforeach?>
                </div>
                <?if (!empty($arResult["data"])):?>
                    
                    <?foreach ($arResult["data"] as $key => $data):?>
                        <div class="table-row list" list-id="<?=$key?>">
                            <?foreach ($arResult["COLUMN"] as $column => $type):?>
                                <?if ($type == "FILE"):?>
                                    <div class="table-item">
                                        <span>
                                            <a class="word" href="<?=$data[$column]?>" target="_blank"><?=$data[$column]?></a>
                                        </span>
                                    </div>   
                                <?else:?>
                                    <div class="table-item">
                                        <span>
                                            <?if ($type == "LIST"):?>
                                                <?if (!empty($arResult["foreginData"][$column])):?>
                                                    <?foreach ($arResult["foreginData"][$column] as $foreginData):?>
                                                    <?if ($foreginData[$arResult["foregin"][$column]["column"]] == $data[$column]):?><?=$foreginData["name"]?><?endif?>
                                                    <?endforeach?>
                                                <?endif?>
                                            <?else:?>
                                                <?=$data[$column]?>
                                            <?endif?>
                                        </span>
                                    </div> 
                                <?endif?>
                            <?endforeach?>
                        </div>
                        <form class="table-row form" form-id="<?=$key?>" data-id="<?=$key?>">
                            <?foreach ($arResult["COLUMN"] as $column => $type):?>
                                <div class="table-item">
                                    <?if ($type == "TEXT"):?>
                                        <input class="w-100" type="text" name="<?=$column?>" value="<?=$data[$column]?>">
                                    <?endif?>
                                    <?if ($type == "FILE"):?>
                                        <input class="w-100" type="text" name="<?=$column?>" value="<?=$data[$column]?>">
                                    <?endif?>
                                    <?if ($type == "DATE"):?>
                                        <input class="w-100 calendar" type="text" name="<?=$column?>" value="<?=$data[$column]?>">
                                    <?endif?>
                                    <?if ($type == "HIDDEN"):?>
                                        <input class="w-100" type="text" disabled name="<?=$column?>" value="<?=$data[$column]?>">
                                        <input class="w-100" type="text" hidden name="<?=$column?>" value="<?=$data[$column]?>">
                                    <?endif?>
                                    <?if ($type == "LIST"):?>
                                        <select class="w-100" name="<?=$column?>">
                                            <?foreach ($arResult["foreginData"][$column] as $foreginData):?>
                                                <option value="<?=$foreginData[$arResult["foregin"][$column]["column"]]?>" <?if ($foreginData[$arResult["foregin"][$column]["column"]] == $data[$column]):?>selected<?endif?>><?=$foreginData["name"]?></option>
                                            <?endforeach?>
                                        </select>
                                    <?endif?>
                         
                                </div>
                            <?endforeach?>
                            <div class="edit-value">
                                <input class="submit-item" type="hidden" name="SHOW_TEMPLATE" value="0">
                                <input type="submit" class="save" form-id="<?=$key?>" value="Сохранить">
                                <input type="submit" class="delete" form-id="<?=$key?>" value="Удалить">
                                <input type="submit" class="cancel" form-id="<?=$key?>" value="Отмена">
                            </div>
                        </form>
                    <?endforeach?>
                <?endif?>
            </div>
        </div>
    </div>
    <script>
        var tabledata;
        $(document).ready(function(){
            tabledata = new tableData(<?=json_encode($arResult)?>);
        });

        $('.calendar').daterangepicker({
            inline: true,
            singleDatePicker: true,
            startDate: moment().subtract(2, 'days'),
            endDate: moment(),
            autoApply: true,

            minDate: moment().subtract(0, 'days'),
            "locale": {
                "format": "DD.MM.YYYY",
                "separator": " - ",
                "fromLabel": "From",
                "toLabel": "To",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": ["ВС", "ПН", "ВТ",  "СР","ЧТ", "ПТ", "СБ" ],
                "monthNames": ["Январь","Февраль","Март","Апрель", "Май", "Июнь","Июль","Август","Сентябрь","Октябрь", "Ноябрь","Декабрь"],
                "firstDay": 1
            },
        });
        $(document).ready(function () {
            $('.daterangepicker.dropdown-menu.ltr.show-calendar.opensright').show();
        });
    </script>
<?endif?>


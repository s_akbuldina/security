<?  
    global $config;
    $orm = new ORM();

    $componentTamplate = "template";
    if (!empty($this->params["handler"])){
        $componentTamplate = $this->params["handler"];
    } 
    if (isset($this->params["request"])){
        $handler = $this->params["request"]["handler"];
        $arFiels = $this->params["request"];
        $table = $arFiels["table"];
        $id = $arFiels["id"];

        if ($handler == "update"){
            
            unset($arFiels["handler"]);
            unset($arFiels["id"]);
            unset($arFiels["table"]);
            echo $orm->updateTableByID($table, $arFiels, $id);
            die();
        }
        if ($handler == "add"){
            $uploaded = array();

            if(!empty($_FILES)) {         
                foreach ($_FILES as $field => $file){
                    foreach($file as $position => $name) {
                 
                        $name = str_replace(" ", "", $name);
                        if(move_uploaded_file($_FILES[$field]['tmp_name'], $_SERVER["DOCUMENT_ROOT"].'/upload/'.$name)) {
                            $uploaded[$field] = array(
                                'name' => $name,
                                'src' => '/upload/'.$name
                            );
                        }
                    }
                }
            }
           

            foreach ($uploaded as $field => $value){
                $arFiels[$field] = $value["src"];
            }

            unset($arFiels["handler"]);
            unset($arFiels["table"]);
            unset($arFiels["SHOW_TEMPLATE"]);
            echo json_encode($arFiels);
            $orm->insertIntoTable($table, $arFiels);
            die();
        }
        if ($handler == "delete"){

            $orm->deleteFromTable($table, $arFiels["id"]);
            die();
        }
    }
    if (isset($this->params["table"])){

        $result = $orm->getList($this->params["table"], [], []);
        
        $lang = $orm->getList("lang_tables", [], ["name" => $this->params["table"]])[0];
        $lang_columns = $orm->getList("lang_columns", [], ["lang_tables" => $this->params["table"]]);
        $file_fields = $orm->getList("file_fields", [], ["table_name" => $this->params["table"]]);
        $date_fields = $orm->getList("date_fields", [], ["table_name" => $this->params["table"]]);
        
        $informationSchema = $orm->getInformationSchema($this->params["table"]);

        
        $keyColumnUsage = $orm->getKeyColumnUsage($this->params["table"]);
        foreach ($keyColumnUsage as $column){
            if (isset($informationSchema[$column["COLUMN_NAME"]])){
                if (!empty($column["REFERENCED_TABLE_NAME"])){
                    $foregin[$column["REFERENCED_TABLE_NAME"]]["column"] = $column["REFERENCED_COLUMN_NAME"];
                    $foreginData[$column["REFERENCED_TABLE_NAME"]] = $orm->getList($column["REFERENCED_TABLE_NAME"], [], []);

                }
            }
        }

        $foregin = $orm->getForegin();

        $this->arResult["columns"] = $informationSchema;
        $this->arResult["foregin"] = $foregin;
        $this->arResult["data"] = $result;
        $this->arResult["lang"] = $lang;
        $this->arResult["foreginData"] = $foreginData;
        $this->arResult["table"] = $this->params["table"];
        if (!empty($lang_columns)){
            foreach ($lang_columns as $key => $columns){
                $lang_columns[$columns["name"]] = $columns;
                unset($lang_columns[$key]);
            }
            foreach ($this->arResult["columns"] as $key => $columns){

                if (!empty($lang_columns[$key])){
                    $this->arResult["lang_columns"][$key] = $lang_columns[$key];
                } else {
                    $this->arResult["lang_columns"][$key]["name"] = $key;
                    $this->arResult["lang_columns"][$key][$config["LANG"]] = $key;
                }
            }
        } else {
            foreach ($this->arResult["columns"] as $key => $columns){
                $this->arResult["lang_columns"][$key] = [
                    $config["LANG"] => $key,
                    "name" => $key,
                ];
            }
        }
        

        foreach ($this->arResult["columns"] as $column => $type){
            if (isset($this->arResult["foregin"][$column])){
                $this->arResult["COLUMN"][$column] = "LIST";
            } else {
                $this->arResult["COLUMN"][$column] = "TEXT";
            }
            if ($column == "id"){
                $this->arResult["COLUMN"][$column] = "HIDDEN";
            }
        }
         
        if (!empty($file_fields)){
            foreach ($file_fields as $fields){
                if (!empty($this->arResult["COLUMN"][$fields["name"]])){
                    $this->arResult["COLUMN"][$fields["name"]] = "FILE";
                }
            }
        }

        if (!empty($date_fields)){
            foreach ($date_fields as $fields){
                if (!empty($this->arResult["COLUMN"][$fields["name"]])){
                    $this->arResult["COLUMN"][$fields["name"]] = "DATE";
                }
            }
        }
       
        if (!empty($this->arResult["lang"][$config["LANG"]])){
            $this->arResult["LANG"] = $this->arResult["lang"][$config["LANG"]];
        } else {
            $this->arResult["LANG"] = $this->arResult["lang"]["name"];
        }
    }
   
    if (empty($_REQUEST)){
        $this->arResult["add_url"] = $_SERVER["REQUEST_URI"]."?handler=add";
        $this->arResult["update_url"] = $_SERVER["REQUEST_URI"]."?handler=update";
        $this->arResult["delete_url"] = $_SERVER["REQUEST_URI"]."?handler=delete";
    } else {
        $this->arResult["add_url"] = $_SERVER["REQUEST_URI"]."&handler=add";
        $this->arResult["update_url"] = $_SERVER["REQUEST_URI"]."&handler=update";
        $this->arResult["delete_url"] = $_SERVER["REQUEST_URI"]."&handler=delete";
    }
    $this->arResult["CurPage"] = $_SERVER["REQUEST_URI"];
    $access_table = $orm->getList("access_table", [], ["user_group" => $_SESSION["user_group"], "name" => $this->params["table"]])[0];
    $access = $orm->getList("access", [], ["id" => $access_table["access"]])[0];
    $this->arResult["access"] = $access["attribute"];
    $this->arResult["default_values"]["user"] = $_SESSION["user_id"];
    //unset($this->arResult["COLUMN"]["id"]);
    $this->arResult["COLUMN"]["user"] = "HIDDEN";
    if (!(empty($access)) && $access["attribute"] != "D"){
        if ($access["attribute"] == "W"){
            $componentTamplate = "add";
        }
        if ($access["attribute"] == "R"){
            $componentTamplate = "template";
        }
        $this->includeTemplateComponents($componentTamplate);
    } else {
        echo "Доступ к ресурсу запрещен";
    }
    
?>
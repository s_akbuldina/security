<?  
    global $defaultValues;
    $defaultValues = [];
    $this->includeTemplateComponents();
    $core = new Core();
    $core->includeInstallFiles();

    global $connectionsFromTables;         
    global $installTables;
    global $dbconnect;
    
    $ConstructDB = new ConstructDB();
    $ConstructDB->tables = $installTables;
    $ConstructDB->connectionsFromTables = $connectionsFromTables;
    $ConstructDB->defaultValues = $defaultValues;
    $ConstructDB->install($dbconnect["database"]);


?>
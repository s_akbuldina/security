$(document).ready(function(){
    $(".auth-form .submit-item").on("click", function(){
        event.preventDefault();
        var data = $('.auth-form').serializeArray();
       
        $.ajax({
            url: "/auth/index.php",
            type: 'post',
            data: data,
            success: function(result) {
                if (result.status == "success"){
                    window.location.href = "/";
               } else {
                   $(".status").text("Ошибка авторизации");
               }
            }
          })
    });
  
});
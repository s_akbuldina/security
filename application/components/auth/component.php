<?  
    if (isset($_REQUEST["login"])){
        header('Content-type: application/json; charset=utf-8');
        global $User;
        //$User->authorizeUser(1);
        $login = $_REQUEST["login"];
        $password = $_REQUEST["password"];
        $result = $User->authorize($login, $password);
        
        echo json_encode($result);
    } else {
        $this->includeTemplateComponents();
    }
   
?>
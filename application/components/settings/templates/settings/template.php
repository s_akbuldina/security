<?if (!empty($arResult)):?>
    <div class="settings">
        <div class="settings-menu">
            <?foreach ($arResult["menu"] as $menu):?>
                <a href="<?=$_SERVER["SCRIPT_NAME"].$menu["url"]?>">
                    <div class="menu-item">
                        <span>
                            <?=$menu["name"]?>
                        </span>
                    </div>
                </a>
            <?endforeach?>
        </div>
        <div class="settings-content">
                <?    
                    global $components;
                    $components->includeComponents("table", "table", ["table" => $_REQUEST["table"], "handler" => $_REQUEST["handler"]]);
                ?>
        </div>
    </div>
<?endif?>
<?
    global $User;


?>
<div class="menu-container">
    <div class="menu">
        <div class="menu-items">
            <a href="/">
                <div class="menu-item">
                    <span>
                        Главная
                    </span>
                </div>
            </a>
            <a href="/deal/">
                <div class="menu-item">
                    <span>
                        Заявки
                    </span>
                </div>
            </a>
            <a href="/acceptance/">
                <div class="menu-item">
                    <span>
                        Приём материалов
                    </span>
                </div>
            </a>
            <a href="/itd/">
                <div class="menu-item">
                    <span>
                        ИТД изготовленных заготовок
                    </span>
                </div>
            </a>
            <?if ($User->isAdmin()):?>
                <a href="/settings/">
                    <div class="menu-item">
                        <span>
                            Настройки
                        </span>
                    </div>
                </a>
            <?endif?>
            <a href="?logout=yes">
                <div class="menu-item">
                    <span>
                        Выйти
                    </span>
                </div>
            </a>
        </div>
    </div>
</div>